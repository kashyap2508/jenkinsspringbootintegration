package com.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.main.controller.Controller;

@RunWith(SpringRunner.class)//junit5
@SpringBootTest//Testcase inside springboot
@ContextConfiguration(classes=TestBeanConfig.class)//componentscan
public class ControllerTest {
	private MockMvc mvc;
	@Before
	public void setup(){
		mvc=MockMvcBuilders.standaloneSetup(new Controller()).build();
	}
	@Test
	public void loginTest() throws Exception
	{
		mvc.perform(MockMvcRequestBuilders.get("/loginPage"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.view().name("login"));
	}
	@Test
	public void successTest() throws Exception
	{
		mvc.perform(MockMvcRequestBuilders.get("/succes"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.view().name("success"));
	}
}
