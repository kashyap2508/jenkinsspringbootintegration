package com.main.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;

import com.main.model.User;


@org.springframework.stereotype.Controller
public class Controller {
	@GetMapping("/loginPage")
	public ModelAndView loginPage(@ModelAttribute User user){
		return new ModelAndView("login");
	}
	@GetMapping("/succes")
	public ModelAndView successPage(@ModelAttribute User user){
		ModelAndView modelAndView = new  ModelAndView("success");
		modelAndView.addObject("user", user);
		return modelAndView;
	}
}